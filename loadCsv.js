var fs = require('fs');
var csv = require('fast-csv');

module.exports.lastLine = function(fileLocation, line, cb) {
    var file = [];
    var stream = fs.createReadStream(fileLocation);
    csv
        .fromStream(stream)
        .on("data", function(data){
            file.push(data);
        })
        .on("end", function(n){
            cb(file[n - line]);
        });
};