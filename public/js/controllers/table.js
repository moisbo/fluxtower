'use strict';

angular.module('app')
    .controller('TableCtrl', function ($scope, $state, $http) {

        $scope.tableName = $state.params.name;
        $scope.items = {};
        $scope.headers = {};
        $scope.first =  0;
        $scope.last =  0;
        $scope.loaded = false;
        $scope.error = false;

        var tableDetails = $http.get('pragma/'+$scope.tableName)
            .success(function(res){
                $scope.headers = res.rowDefs;
            })
            .error(function(error){
                $scope.error = true;
            });
        var tableData = $http.get('flux/'+$scope.tableName)
            .success(function (res) {
                $scope.loaded = true;
                $scope.items = res.rows;
                $scope.first =  res.first;
                $scope.last =  res.last;
            })
            .error(function(error){
                $scope.error = true;
            });
        $scope.paginate = function(isNext){
            $scope.loaded = false;
            if (isNext) {
                var betweenFirst = parseInt($scope.first) + 100;
                var betweenLast = parseInt($scope.last) + 100;
            } else {
                var betweenFirst = $scope.first - 100;
                var betweenLast = $scope.last - 100;
            }
            $http.get('/flux/'+ $scope.tableName + '/' + betweenFirst + '/' + betweenLast)
                .success(function (res) {
                    $scope.loaded = true;
                    $scope.items = res.rows;
                    $scope.first =  res.first;
                    $scope.last =  res.last;
                });
        };
    });
