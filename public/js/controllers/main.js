'use strict';

angular.module('app')
    .controller('MainCtrl', function ($scope, $http) {
        $http.get('/tables')
            .success(function(res){
                $scope.tables = res;
            });
    });
