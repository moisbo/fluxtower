'use strict';

angular.module('app').config(function($urlRouterProvider, $stateProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('main',{
            url:'/',
            templateUrl:'main.html',
            controller:'MainCtrl'
        })
        .state('table', {
            url:'/table/:name',
            templateUrl:'table.html',
            controller:'TableCtrl'
        });
});