var expect = require('chai').expect;
var loadCsv = require('../loadCsv.js');
var db = require('../db.js');


describe('Event Model', function () {

    it('it should not be empty', function() {
        loadCsv.lastLine('/Users/Moy/Documents/fluxtower/test-data/CR3000_slow_flux.dat', function (last) {
            last.should.not.be.equal(undefined);
        });
    });
    it('it should not be empty', function() {
        db.getRowsBetween('CR3000_slow_flux', 'RECORD', 41300, 41367, function(rows){
            expect(rows).not.be.equal(undefined);
        });
    });
    it('it should not be empty', function() {
        db.getLastPoint('CR3000_slow_flux', 'RECORD', function(row){
            expect(row).not.be.equal(undefined);
        });
    });
});

describe('Inserting', function(){
    var lastRow = {};
    before(function (done) {
        loadCsv.lastLine('/Users/Moy/Documents/fluxtower/test-data/file.dat', function (last) {
            lastRow = last;
            done();
        });
    });
    it('should insert', function(){
        db.insertRow('CR3000_slow_flux', lastRow, function (err) {
            expect(last).be.equal(undefined);
        });
    });
    it('should be larger', function(){
        db.getLastPoint('CR3000_slow_flux', 'RECORD', function (row) {
            expect(row.RECORD).be.equal(41368);
        });
    });
});
