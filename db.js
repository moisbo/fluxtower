var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('ozflux_serf');
var Promise = require('bluebird');

module.exports.getRows = function(last, table, order, cb){
    db.all("SELECT * FROM " + table + ' ORDER BY ' + order + ' DESC LIMIT '+ last , function(err, rows) {
        cb(rows);
    });
};
module.exports.getPragma = function(table){
    return new Promise(function(resolve, reject) {
        db.all('PRAGMA table_info(' + table + ')', function (err, rows) {
            if(err){
                reject(err);
            }else {
                resolve(rows);
            }
        });
    });
};
module.exports.getRowsBetween = function(table, column, betweenFirst, betweenLast, cb){
    db.all('SELECT * FROM ' + table + ' WHERE '+ column +' BETWEEN ' + betweenFirst + ' AND ' + betweenLast +' ', function(err, rows) {
        cb(rows);
    });
};
module.exports.getLastPoint = function(table, column, cb){
    db.each('SELECT '+ column + ' FROM ' + table + ' ORDER BY ' + column + ' DESC LIMIT 1', function(err, row){
        if(err){
            cb(null);
        }else {
            cb(row);
        }
    });
};
module.exports.insertRow = function(table, row, record, cb){
    var insert = '';
    for(var i = 0; i < row.length;i++){
        insert +=  "'" + row[i] + "'";
        if(i<row.length - 1) insert += ',';
    }
    module.exports.getRecord(table, record, function(row){
        if(row.length <= 0){
            db.run('INSERT INTO ' + table + ' VALUES ('+insert+')', function(err){
                cb(err);
            });
        }
    });
};
module.exports.getRecord = function(table, record, cb){
    db.all('SELECT RECORD FROM ' + table + ' WHERE RECORD = ' + record + ' LIMIT 1', function(err, row){
        if(!row){
            cb([]);
        }else{
            cb(row);
        }
    });
};
module.exports.getTables = function(){
    return new Promise(function(resolve, reject) {
        db.all("SELECT name FROM sqlite_master WHERE type='table';", function (err, rows) {
            if (err) {
                reject(err)
            } else {
                resolve(rows);
            }
        })
    });
};