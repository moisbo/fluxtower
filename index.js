var express  = require('express');
var bodyParser = require('body-parser');
var loadCsv = require('./loadCsv.js');
var db = require('./db.js');
var CronJob = require('cron').CronJob;
var waterfall = require('async-waterfall');
var _ = require('underscore');
var settings = require('./settings.js');
var moment = require('moment');

var app = express();
app.use(bodyParser.json());
app.use(express.static('./public'));

var set = {};

if(settings.app.dev){
    set.port = settings.dev.port;
    set.cronTime = settings.dev.cronTime;
    set.dropboxDir = settings.dev.dropboxDir;
}else{
    set.port = settings.app.port;
    set.cronTime = settings.app.cronTime;
    set.dropboxDir = settings.app.dropboxDir;
}

app.get('/flux/:table', function (req, res) {
    db.getLastPoint(req.params.table, 'RECORD', function(row){
        db.getRowsBetween(req.params.table, 'RECORD', row.RECORD - 100, row.RECORD, function (rows) {
            res.status(200).send({
                rows: rows,
                first: row.RECORD - 100,
                last: row.RECORD,
                lastInsert: row.RECORD
            });
        });
    });
});

app.get('/flux/:table/:first/:last', function (req, res) {
    db.getRowsBetween(req.params.table, 'RECORD', req.params.first, req.params.last, function (rows) {
        res.status(200).send({
            rows: rows,
            first: req.params.first,
            last: req.params.last
        });
    });
});

app.get('/pragma/:table', function(req, res){
    var table = _.findWhere(settings.tables, {name: req.params.table});
    db.getPragma(table.name)
        .then(function (rows) {
            res.status(200).send({
                rows: rows,
                rowDefs: table.rows
            });
        })
        .catch(function(err){
            res.status(400).send({rows:{},rowsDefs:{}})
        });
});

app.get('/tables', function(req, res){
    db
        .getTables()
        .then(function(tables){
            res.status(200).send(tables);
        })
        .catch(function(err){
            res.status(400).send({})
        });
});

app.get('/lastday/:table', function(req, res){
    db.getLastPoint(req.params.table, 'TIMESTAMP', function(row){
        try{
            var timestamp = row.TIMESTAMP.trim();
            var current = moment(timestamp).format("YYYY-MM-DD HH:MM:SS");
            var today = moment(new Date());
            var diff = today.diff(current, 'days');
            var result = '';
            if(diff > 1){
                result = 'Last day updated ' + diff + ' days ago on ' + current;
            }else{
                result = 'OK';
            }
            res.status(200).send(result);
        }
        catch(err){
            console.log(Date() + " " + err);
            res.status(200).send(err);
        }
    });
});

var server = app.listen(set.port, function(){
    console.log(Date() + ' Api listening on ', server.address().port);
});

var job = new CronJob({
    cronTime: set.cronTime,
    onTick: function() {
        waterfall([
            function(callback) {
                getLineAndInsert(2);
                callback(null, '1');
            },
            function (arg1, callback) {
                getLineAndInsert(1);
                callback(null, 'done');
            }],function(err, result){
                if(err){
                    console.log(Date() + " " + err);
                }
        });
    },
    start: false,
    timeZone: 'Australia/Brisbane'
});

job.start();

function getLineAndInsert(line){
    for(var i = 0; i < settings.tables.length; i++) {
        insertAndReturn(set.dropboxDir + settings.tables[i].file, settings.tables[i].name, line, function(){
        });
    }
 }

function insertAndReturn(location, name, line, done){
    loadCsv.lastLine(location, line, function (lastRow) {
        db.insertRow(name, lastRow, lastRow[1], function (err) {
            if (err) {
                console.log(Date() + " " + err);
                done();
            }
            done();
        });
    });
}